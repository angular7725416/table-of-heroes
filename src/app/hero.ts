export interface Hero {
  id: number;
  name: string;
  sex: 'MALE' | 'FEMALE';
}

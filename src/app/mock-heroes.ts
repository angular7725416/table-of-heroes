import { Hero } from './hero';

export const HEROES: Hero[] = [
  { id: 11, name: 'Dr Nice', sex: 'MALE' },
  { id: 12, name: 'Narco', sex: 'MALE' },
  { id: 13, name: 'Bombasto', sex: 'MALE' },
  { id: 14, name: 'Celeritas', sex: 'MALE' },
  { id: 15, name: 'Magneta', sex: 'FEMALE' },
  { id: 16, name: 'RubberMan', sex: 'MALE' },
  { id: 17, name: 'Dynama', sex: 'FEMALE' },
  { id: 18, name: 'Dr IQ', sex: 'MALE' },
  { id: 19, name: 'Magma', sex: 'FEMALE' },
  { id: 20, name: 'Tornado', sex: 'MALE' }
];

import {Component, OnInit} from '@angular/core';
import {HeroListService} from '../hero-list.service';

@Component({
  selector: 'app-hero-table-actions',
  templateUrl: './hero-table-actions.component.html',
  styleUrls: ['./hero-table-actions.component.css']
})
export class HeroTableActionsComponent implements OnInit {

  constructor(private heroListService: HeroListService) { }

  ngOnInit(): void {
  }

  hasAllSelected(): boolean {
    return this.heroListService.hasAllSelected();
  }

  hasIntermediate(): boolean {
    return this.heroListService.hasSelected() && !this.heroListService.hasAllSelected();
  }

  toogle(): void {
    if (this.hasAllSelected()) {
      this.heroListService.unselectAll();
    } else {
      this.heroListService.selectAll();
    }
  }

  invertSelection(): void {
    this.heroListService.invert();
  }

  sizeSelectedAll(): number {
    return this.heroListService.sizeSelectedAll();
  }

  sizeAll(): number {
    return this.heroListService.sizeALl();
  }

  sizeSelectedMale(): number {
    return this.heroListService.getFilteredSelectedSize( this.IS_MALE);
  }

  sizeMale(): number {
    return this.heroListService.getFilteredSize( this.IS_MALE);
  }

  sizeSelectedFemale(): number {
    return this.heroListService.getFilteredSelectedSize( this.IS_FEMALE);
  }

  hasIntermediateMale(): boolean {
    return (this.sizeSelectedMale() > 0) && (this.sizeSelectedMale() !== this.sizeMale());
  }

  toogleMale(): void {
    if (this.sizeSelectedMale() === this.sizeMale()) {
      this.heroListService.unselectFiltered(this.IS_MALE);
    } else {
      this.heroListService.selectFiltered(this.IS_MALE);
    }
  }

  sizeFemale(): number {
    return this.heroListService.getFilteredSize( this.IS_FEMALE);
  }

  hasIntermediateFemale(): boolean {
    return (this.sizeSelectedFemale() > 0) && (this.sizeSelectedFemale() !== this.sizeFemale());
  }

  toogleFemale(): void {
    if (this.sizeSelectedFemale() === this.sizeFemale()) {
      this.heroListService.unselectFiltered(this.IS_FEMALE);
    } else {
      this.heroListService.selectFiltered(this.IS_FEMALE);
    }
  }

  IS_MALE = (hero: { sex: string; }) => hero.sex === 'MALE';

  IS_FEMALE = (hero: { sex: string; }) => hero.sex === 'FEMALE';

}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HeroTableActionsComponent } from './hero-table-actions.component';

describe('HeroTableActionsComponent', () => {
  let component: HeroTableActionsComponent;
  let fixture: ComponentFixture<HeroTableActionsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HeroTableActionsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HeroTableActionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

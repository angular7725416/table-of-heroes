import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HeroTableComponent} from './hero-table/hero-table.component';

const routes: Routes = [
  { path: '', redirectTo: '/table', pathMatch: 'full' },
  { path: 'table', component: HeroTableComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

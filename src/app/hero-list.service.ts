import { Injectable } from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {Hero} from './hero';
import {SelectionModel} from '@angular/cdk/collections';

@Injectable({
  providedIn: 'root'
})
export class HeroListService {

  private sourceList: Hero[];

  private selectedSubject: Subject<Hero[]> = new Subject<Hero[]>();

  private selection: SelectionModel<Hero> = new SelectionModel<Hero>(true, []);

  constructor() { }

  public getSelected(): Observable<Hero[]> {
    return this.selectedSubject.asObservable();
  }

  public initList(heroes: Hero[]): void {
    this.sourceList = heroes;
    this.selection.clear();
    this.signalNewSelected();
  }

  public sizeALl(): number {
    return this.sourceList.length;
  }

  public sizeSelectedAll(): number {
    return this.selection.selected.length;
  }

  selectAll(): void {
    this.selection.select(...this.sourceList);
    this.signalNewSelected();
  }

  unselectAll(): void {
    this.selection.clear();
    this.signalNewSelected();
  }

  hasAllSelected(): boolean {
    return this.sourceList && this.sourceList.length === this.selection.selected.length;
  }

  hasSelected(): boolean {
    return this.selection.hasValue();
  }

  public toogle(hero: Hero): void {
    this.selection.toggle(hero);
    this.signalNewSelected();
  }

  public toogleFiltered(predicate: (hero) => boolean): void {
    this.sourceList.filter(predicate).forEach( hero =>  this.selection.toggle(hero));
    this.signalNewSelected();
  }

  invert(): void {
    const currentSelection = this.selection.selected;
    this.selection.clear();
    this.selection.select(...this.sourceList.filter( hero => !currentSelection.includes(hero)));
    this.signalNewSelected();
  }

  private signalNewSelected(): void {
    this.selectedSubject.next(this.selection.selected);
  }

  getFilteredSelectedSize(predicate: (hero) => boolean): number {
    return this.selection.selected.filter(predicate).length;
  }

  getFilteredSize(predicate: (hero) => boolean): number {
    return this.sourceList.filter(predicate).length;
  }

  selectFiltered(predicate: (hero) => boolean): void {
    this.selection.select(...this.sourceList.filter(predicate));
    this.signalNewSelected();
  }

  unselectFiltered(predicate: (hero) => boolean): void {
    this.selection.deselect(...this.sourceList.filter(predicate));
    this.signalNewSelected();
  }
}

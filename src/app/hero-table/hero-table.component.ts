import {Component, OnDestroy, OnInit} from '@angular/core';
import { HeroService } from '../hero.service';
import {Hero} from '../hero';
import {Subscription} from 'rxjs';
import {HeroListService} from '../hero-list.service';

@Component({
  selector: 'app-hero-table',
  templateUrl: './hero-table.component.html',
  styleUrls: ['./hero-table.component.css']
})
export class HeroTableComponent implements OnInit, OnDestroy {

  heroes: Hero[];

  private subscription: Subscription;

  private selectedSub: Subscription;

  displayedColumns: string[] = ['select', 'id', 'name', 'sex'];
  private selected: Hero[];

  constructor(private heroService: HeroService,
              private heroListService: HeroListService) { }

  ngOnInit(): void {
    this.subscription = this.heroService.getHeroes().subscribe(
      heroes => {
        this.heroes = heroes;
        this.heroListService.initList(this.heroes);
      }
    );
    this.selectedSub = this.heroListService.getSelected().subscribe( newSelected => {
      this.selected = newSelected;
    });
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
    if (this.selectedSub) {
      this.selectedSub.unsubscribe();
    }
  }

  toggleSelection(hero: Hero): void {
    this.heroListService.toogle(hero);
  }

  public isSelected(hero: Hero): boolean {
    return this.selected && this.selected.includes(hero);
  }

  getSelected(): Hero[] {
    return this.selected;
  }
}
